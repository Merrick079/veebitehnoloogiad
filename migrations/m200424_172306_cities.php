<?php

use yii\db\Migration;

/**
 * Class m200424_172306_cities
 */
class m200424_172306_cities extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = file_get_contents(__DIR__. '/city.sql');
        $command = Yii::$app->db->createCommand($sql);
        $command->execute();

        while ($command->pdoStatement->nextRowSet()) {}
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200424_172306_cities cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_172306_cities cannot be reverted.\n";

        return false;
    }
    */
}
