<?php

use yii\db\Migration;

/**
 * Class m200424_164554_mainor
 */
class m200424_164554_mainor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news',
        [
            'id' => \yii\db\Schema::TYPE_PK,
            'title' => \yii\db\Schema::TYPE_STRING,
            'content' => \yii\db\Schema::TYPE_TEXT
        ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m200424_164554_mainor cannot be reverted.\n";
        $this->dropTable('news');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_164554_mainor cannot be reverted.\n";

        return false;
    }
    */
}
