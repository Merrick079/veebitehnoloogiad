<?php

use yii\db\Migration;

/**
 * Class m200424_170843_mainor2
 */
class m200424_170843_mainor2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('news', [
            'title' => 'Test 1',
            'content' => 'Content 1'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m200424_170843_mainor2 cannot be reverted.\n";
        $this->delete('news', ['id' => 1]);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_170843_mainor2 cannot be reverted.\n";

        return false;
    }
    */
}
