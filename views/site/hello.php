<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->registerJsFile('@web/js/basic.js', ['depends' => 'yii\web\YiiAsset']);

?>
<h1>Hello <?php echo $name2;  ?></h1>
<h3>This is my first project on Yii2</h3>
<div id='name'>...</div>

<?php
    $form = ActiveForm::begin();

    echo $form->field($model, 'firstname')->label('Имя')->input('text');
    echo $form->field($model, 'lastname')->label("фамилия")->input('text');
    echo $form->field($model, 'text')->label('текст')->textarea(['rows' => 10, 'cols' => 5]);

    echo Html::button('send',['type'=>'submit', 'class' => 'btn btn-success']);

    ActiveForm::end();



?>



<!--

<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-secondary">Secondary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-light">Light</button>
<button type="button" class="btn btn-dark">Dark</button>

<button type="button" class="btn btn-link">Link</button>
-->

<?php

    echo '<ul>';
    for ($i = 0; $i < 3; $i++)
    {
        echo '<li>' .$names[$i]. '</li>';
    }
    echo '</ul>';

    echo '<br><br><br>';

    echo '<ul>';
    foreach ($names as $key => $value)
    {
        echo '<li>' . $names[$key] . '(' . $value.') </li>';
    }
    echo '</ul>';
?>