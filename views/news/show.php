<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->registerJsFile('@web/js/news.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('@web/css/news.css', ['depends' => 'yii\web\YiiAsset']);
?>

<h1>Nintendo Switch Still Sold Out Almost Everywhere</h1>
<h3>It's becoming increasingly difficult to find stock of the original Nintendo Switch at its official, $300 retail
    price across the usual channels, including Amazon, Best Buy, and Ebay. The Nintendo Switch Lite, which retails for
    $200, isn't as scarce across all retailers, but it is sold out on Amazon. Nintendo of America knows supply is short,
    and hopes to renew stock in the near future. "Nintendo Switch hardware is selling out at various retail locations in
    the U.S., but more systems are on the way," Nintendo explained in a statement to GameSpot. "We apologize for any
    inconvenience."</h3>


<?php
$form = ActiveForm::begin();

echo $form->field($model, 'zagolovok')->label('Заголовок')->input('text');
echo $form->field($model, 'telo')->label('Тело новости')->textarea(['rows' => 10, 'cols' => 5]);

echo Html::button('send',['type'=>'submit', 'class' => 'btn btn-success']);

ActiveForm::end();



?>