<?php


namespace app\models;

use yii\base\Model;

class NewsForm extends Model
{
    public $zagolovok;
    public $telo;

    public function rules()
    {
        return [
            ['zagolovok', 'required'],
            ['telo', 'required']
        ];
    }
}